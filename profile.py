#!/usr/bin/python

"""


"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.emulab.spectrum as spectrum
import geni.rspec.emulab.route as route
import geni.rspec.igext as ig

x310_node_disk_image = \
        "urn:publicid:IDN+emulab.net+image+raytheonmlfec:fpga-profile.node-node1"





portal.context.defineParameter("x310_comp_nodetype",
                               "Type of the node paired with the X310 Radios",
                               portal.ParameterType.STRING, "d430", ("d430", "d740", "d710", "dl360"))




portal.context.defineParameter("start_vnc", 
                               "Start X11 VNC on all compute nodes",
                               portal.ParameterType.BOOLEAN, True)

# Bind parameters
params = portal.context.bindParameters()

# Get request object
request = portal.context.makeRequestRSpec()


# Declare that we may be starting X11 VNC on the compute nodes.
if params.start_vnc:
    request.initVNC()



for i in range(2):
    node = request.RawPC("%s-node" % "node" + str(i))
    node.hardware_type = params.x310_comp_nodetype
    node.disk_image = x310_node_disk_image
    #node.addService(rspec.Execute(shell="bash",
     #                             command="sudo chmod +x /local/repository/setup_vnc.sh"))
    #node.addService(rspec.Execute(shell="bash",
     #                             command="sudo /local/repository/setup_vnc.sh"))

    node.addService(rspec.Execute(shell="bash",
                                  command="sudo chmod +x /local/repository/start_vnc.sh"))
    node.addService(rspec.Execute(shell="bash",
                                  command="sudo /local/repository/start_vnc.sh"))
    if params.start_vnc:
        node.startVNC(True)

portal.context.printRequestRSpec()
